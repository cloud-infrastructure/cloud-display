var w = 1000,
    h = 800,
    node,
    link,
    root;

var win = window,
    doc = document,
    e = doc.documentElement,
    g = doc.getElementsByTagName('body')[0],
    x = win.innerWidth || e.clientWidth || g.clientWidth,
    y = win.innerHeight|| e.clientHeight|| g.clientHeight;

var force = d3.layout.force()
    .on("tick", tick)
    .charge(function(d) {
        return d._children ? -d.size / 100 : -50;
    })
    .linkDistance(function(d) {
        return d.target._children ? 80 : 50;
    })
    .size([x, y]);

var svg = d3.select("body").append("svg:svg")
    .attr("width", x)
    .attr("height", y);

var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([-10, 0])
    .html(function(d) {
        return d.name;
    });
svg.call(tip);

d3.json("../html/topo.json", function(json) {
    root = json;
    root.fixed = true;
    root.x = w / 2;
    root.y = h / 2 - 80;
    flatten(root); //to set ids
    collapseAll(root);
    root.children = root._children;
    root._children = null;
    update();
});

function collapseAll(d){
    if (d.children){
        d.children.forEach(collapseAll);
        d._children = d.children;
        d.children = null;
    }
    else if (d._childred){
        d._children.forEach(collapseAll);
    }
}
function setParents(d, p){
    d._parent = p;
  if (d.children) {
      d.children.forEach(function(e){ setParents(e,d);});
  } else if (d._children) {
      d._children.forEach(function(e){ setParents(e,d);});
  }
}
function update() {
    var nodes = flatten(root),
        links = d3.layout.tree().links(nodes);

    // Restart the force layout.
    force
        .nodes(nodes)
        .links(links)
        .start();

    // Update the links…
    link = svg.selectAll("line.link")
        .data(links, function(d) {
            return d.target.id;
        });

    // Enter any new links.
    link.enter().insert("svg:line", ".node")
        .attr("class", "link")
        .attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        });

    // Exit any old links.
    link.exit().remove();

    // Update the nodes…
    node = svg.selectAll("circle.node")
        .data(nodes, function(d) {
            return d.id;
        })
        .style("fill", color)
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

    node.transition()
        .attr("r", function(d) {
            return d.children ? 4.5 : Math.sqrt(d.size) / 10;
        });

    // Enter any new nodes.
    node.enter().append("svg:circle")
        .attr("class", "node")
        .attr("cx", function(d) {
            return d.x;
        })
        .attr("cy", function(d) {
            return d.y;
        })
        .attr("r", function(d) {
            return d.children ? 4.5 : Math.sqrt(d.size) / 10;
        })
        .style("fill", color)
        .on("click", click)
        .call(force.drag);

    // Exit any old nodes.
    node.exit().remove();
}

function tick() {
    link.attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        });

    node.attr("cx", function(d) {
            return d.x;
        })
        .attr("cy", function(d) {
            return d.y;
        });
}

function color(d) {
    if (d.status == 'ok')
        return '#81C784';
    else if (d.status == 'error')
        return '#FFF59D';
    return '#e57373';
}

// Toggle children on click.
function click(d) {
    if (d.children) {
        d._children = d.children;
        d.children = null;
    } else {
        d.children = d._children;
        d._children = null;
    }
    tip.hide;
    update();
}

// Returns a list of all nodes under the root.
function flatten(root) {
    var nodes = [],
        i = 0;

    function recurse(node) {
        if (node.children) {
            node.size = node.children.reduce(function(p, v) {
                return p + recurse(v);
            }, 0);
	}
	else if (!node._children)
            node.size = node.data.vcpus + node.data.memory_mb;
        if (!node.id)
	    node.id = node.name;
        nodes.push(node);
        return node.size;
    }

    root.size = recurse(root);
    return nodes;
}
