import json
import getopt
import sys

from mysql.connector import connection

cnx = connection.MySQLConnection(
        user='nova',
        password=sys.argv[1],
        host='dbod-iaas01.cern.ch', port=5508,
        database='nova')

def main():
    cells = get_cells()
    topo = {'name': 'cell00', 'children': [], 'status': 'ok'}
    for c in cells:
        topo_c = {'name': c, 'children': [], 'status': 'ok'}
        for h in cells[c]:
            topo_h = {'name': h, 'children': [], 'status': 'ok'}
            for v in cells[c][h]:
                topo_v = {'name': v, 'data': cells[c][h][v], 'status': get_state('ok', cells[c][h][v]['vm_state'])}
                topo_h['status'] = get_state(topo_h['status'], topo_v['status'])
                topo_h['children'].append(topo_v)
            topo_c['status'] = get_state(topo_c['status'], topo_h['status'])
            topo_c['children'].append(topo_h)
        topo['children'].append(topo_c)
        topo['status'] = get_state(topo['status'], topo_c['status'])

    print json.dumps(topo, indent=2)

def get_cells():
    cursor = cnx.cursor(dictionary=True)
    cursor.execute((
        "SELECT user_id, project_id, image_ref, power_state, vm_state, "
        "memory_mb, vcpus, display_name, host, availability_zone, "
        "uuid, access_ip_v4, "
        "access_ip_v6, task_state, cell_name "
        "FROM instances "
        "WHERE vm_state NOT IN ('building', 'deleted') "
        "LIMIT 1000 "
        )
    )
    errors = []
    cells = {}
    i = 0
    for vm in cursor:
        i += 1
        c = vm['cell_name'].split('!')[1]
        vm['cell_name'] = c
        h = vm['host']
        n = vm['display_name']
        if not cells.has_key(c):
            cells[c] = {}
        if not cells[c].has_key(h):
            cells[c][h] = {}
        cells[c][h][n] = vm

    return cells

def get_state(state1, state2):
    if state1 == 'error' or state2 == 'error':
        return 'error'
    return 'ok'

if __name__ == '__main__':
    main()
